﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;

namespace SafeNote.Core
{
	internal static class ScrambledCrypto
	{
		private const string CryptoKeyVerifyString = "Verified OK";

		public static IList<IBuffer> CreateCryptoKeySet(string scrambler)
		{
#if TEST

			var keyAndIv = new KeyAndIv(
				Tests.EncryptTestConstants.Key.AsBuffer(),
				Tests.EncryptTestConstants.Iv.AsBuffer());
#else

			var keyAndIv = new KeyAndIv(
				CryptographicBuffer.GenerateRandom(32), 
				CryptographicBuffer.GenerateRandom(16));

#endif

			IBuffer scrambledKey = Scrambler.Scramble(keyAndIv.Key, scrambler);
			IBuffer scrambledIv = Scrambler.Scramble(keyAndIv.IV, scrambler);
			IBuffer encryptedVerificationString = Scrambler.Scramble(CryptoSupport.Encrypt(keyAndIv, Encoding.UTF8.GetBytes(CryptoKeyVerifyString).AsBuffer()), scrambler);
			return new List<IBuffer> { scrambledKey, scrambledIv, encryptedVerificationString };
		}

		public static KeyAndIv CreateAlgorithm(IList<IBuffer> cryptoKeySet, string scrambler)
		{
			if (cryptoKeySet.Count < 3)
			{
				throw new ArgumentException("Invalid cryptoKeySet.");
			}

			IBuffer scrambledKey = cryptoKeySet[0];
			IBuffer scrambledIv = cryptoKeySet[1];
			IBuffer encryptedVerificationString = cryptoKeySet[2];

			var key = Scrambler.Scramble(scrambledKey, scrambler);
			var iv = Scrambler.Scramble(scrambledIv, scrambler);
			var keyAndIv = new KeyAndIv(key,iv);

			IBuffer verifyStringBytes;
			try
			{
				verifyStringBytes = CryptoSupport.Decrypt(keyAndIv, Scrambler.Scramble(encryptedVerificationString, scrambler));
			}
			catch
			{
				throw new ArgumentException("Invalid scrambler.");
			}

			string verifyString = CryptographicBuffer.ConvertBinaryToString(BinaryStringEncoding.Utf8, verifyStringBytes);
			if (!verifyString.Equals(CryptoKeyVerifyString))
			{
				throw new ArgumentException("Invalid scrambler.");
			}

			return keyAndIv;
		}
	}
}