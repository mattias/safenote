﻿using System;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace SafeNote.Core
{
	public class Testing
	{
		public void Test()
		{
			// Initialize the encryption process.
			String strMsg = "1234567812345678";     // Data to encrypt.
			String strAlgName = SymmetricAlgorithmNames.AesCbc;
			UInt32 keyLength = 32;                  // Length of the key, in bytes
			BinaryStringEncoding encoding;          // Binary encoding value
			IBuffer iv;                             // Initialization vector
			CryptographicKey key;                   // Symmetric key

			// Encrypt a message.
			IBuffer buffEncrypted = SampleCipherEncryption(
				strMsg,
				strAlgName,
				keyLength,
				out encoding,
				out iv,
				out key);

			// Decrypt a message.
			SampleCipherDecryption(
				strAlgName,
				buffEncrypted,
				iv,
				encoding,
				key);
		}

		public IBuffer SampleCipherEncryption(
			String strMsg,
			String strAlgName,
			UInt32 keyLength,
			out BinaryStringEncoding encoding,
			out IBuffer iv,
			out CryptographicKey key)
		{
			// Initialize the initialization vector.
			iv = null;

			// Initialize the binary encoding value.
			encoding = BinaryStringEncoding.Utf8;

			// Create a buffer that contains the encoded message to be encrypted.
			IBuffer buffMsg = CryptographicBuffer.ConvertStringToBinary(strMsg, encoding);

			// Open a symmetric algorithm provider for the specified algorithm.
			SymmetricKeyAlgorithmProvider objAlg = SymmetricKeyAlgorithmProvider.OpenAlgorithm(strAlgName);

			// Demonstrate how to retrieve the name of the algorithm used.
			String strAlgNameUsed = objAlg.AlgorithmName;

			// Determine whether the message length is a multiple of the block length.
			// This is not necessary for PKCS #7 algorithms which automatically pad the
			// message to an appropriate length.
			if (!strAlgName.Contains("PKCS7"))
			{
				if ((buffMsg.Length % objAlg.BlockLength) != 0)
				{
					throw new Exception("Message buffer length must be multiple of block length.");
				}
			}

			// Create a symmetric key.
			IBuffer keyMaterial = CryptographicBuffer.GenerateRandom(keyLength);
			key = objAlg.CreateSymmetricKey(keyMaterial);

			// CBC algorithms require an initialization vector. Here, a random
			// number is used for the vector.
			if (strAlgName.Contains("CBC"))
			{
				iv = CryptographicBuffer.GenerateRandom(objAlg.BlockLength);
			}

			// Encrypt the data and return.
			IBuffer buffEncrypt = CryptographicEngine.Encrypt(key, buffMsg, iv);
			return buffEncrypt;
		}

		public void SampleCipherDecryption(
			String strAlgName,
			IBuffer buffEncrypt,
			IBuffer iv,
			BinaryStringEncoding encoding,
			CryptographicKey key)
		{
			// Declare a buffer to contain the decrypted data.
			IBuffer buffDecrypted;

			// Open an symmetric algorithm provider for the specified algorithm.
			SymmetricKeyAlgorithmProvider objAlg = SymmetricKeyAlgorithmProvider.OpenAlgorithm(strAlgName);

			// The input key must be securely shared between the sender of the encrypted message
			// and the recipient. The initialization vector must also be shared but does not
			// need to be shared in a secure manner. If the sender encodes a message string
			// to a buffer, the binary encoding method must also be shared with the recipient.
			buffDecrypted = CryptographicEngine.Decrypt(key, buffEncrypt, iv);

			// Convert the decrypted buffer to a string (for display). If the sender created the
			// original message buffer from a string, the sender must tell the recipient what
			// BinaryStringEncoding value was used. Here, BinaryStringEncoding.Utf8 is used to
			// convert the message to a buffer before encryption and to convert the decrypted
			// buffer back to the original plaintext.
			String strDecrypted = CryptographicBuffer.ConvertBinaryToString(encoding, buffDecrypted);
		}
	}
}