﻿using Windows.Storage.Streams;

namespace SafeNote.Core
{
	public class KeyAndIv
	{
		public KeyAndIv(IBuffer key, IBuffer iv)
		{
			Key = key;
			IV = iv;
		}

		public IBuffer Key { get; private set; }

		public IBuffer IV { get; private set; }
	}
}