﻿using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace SafeNote.Core
{
	internal static class CryptoSupport
	{
		public static IBuffer Encrypt(KeyAndIv keyAndIv, IBuffer data)
		{
			SymmetricKeyAlgorithmProvider algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
			CryptographicKey key = algorithm.CreateSymmetricKey(keyAndIv.Key);
			return CryptographicEngine.Encrypt(key, data, keyAndIv.IV);
		}

		public static IBuffer Decrypt(KeyAndIv keyAndIv, IBuffer data)
		{
			SymmetricKeyAlgorithmProvider algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);
			CryptographicKey key = algorithm.CreateSymmetricKey(keyAndIv.Key);
			return CryptographicEngine.Decrypt(key, data, keyAndIv.IV);
		}
	}
}