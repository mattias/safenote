﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace SafeNote.Core
{
	internal static class EncryptedFile
	{
		public const string DefaultExtension = ".safenote";

		private static readonly byte[] FileHeaderBytes = Encoding.UTF8.GetBytes("Safenote");
		private static readonly IBuffer FileHeaderBuffer = FileHeaderBytes.AsBuffer();

		public static IBuffer Encrypt(IBuffer plainTextBuffer, string password)
		{
			IList<IBuffer> cryptoKeySet = ScrambledCrypto.CreateCryptoKeySet(password);

			KeyAndIv keyAndIv = ScrambledCrypto.CreateAlgorithm(cryptoKeySet, password);

			IBuffer encryptedData = CryptoSupport.Encrypt(keyAndIv, plainTextBuffer);

			byte[] output = new byte[FileHeaderBuffer.Length + cryptoKeySet.Sum(x => x.Length + ByteBufferSupport.LengthLength) + encryptedData.Length];

			int index = 0;

			CopyBuffer(FileHeaderBuffer, output, false, ref index);

			foreach (var buffer in cryptoKeySet)
			{
				CopyBuffer(buffer, output, true, ref index);
			}

			CopyBuffer(encryptedData, output, false, ref index);

			return output.AsBuffer();
		}

		public static IBuffer Decrypt(IBuffer encryptedBuffer, string password)
		{
			using (Stream stream = encryptedBuffer.AsStream())
			{
				// Verify file header
				var actualFileHeader = new byte[FileHeaderBuffer.Length];
				stream.Read(actualFileHeader, 0, actualFileHeader.Length);
				if (FileHeaderBytes.Where((t, i) => actualFileHeader[i] != t).Any())
				{
					throw new FormatException();
				}

				IList<IBuffer> buffers = ByteBufferSupport.SplitBuffers(stream, 3);
				if (buffers.Count != 3)
				{
					throw new FormatException();
				}

				KeyAndIv keyAndIv = ScrambledCrypto.CreateAlgorithm(buffers, password);

				var encryptedBytes = new byte[stream.Length - stream.Position];
				stream.Read(encryptedBytes, 0, encryptedBytes.Length);

				IBuffer plainTextBuffer = CryptoSupport.Decrypt(keyAndIv, encryptedBytes.AsBuffer());

				return plainTextBuffer;
			}
		}

		private static void CopyBuffer(IBuffer source, byte[] dest, bool addLength, ref int index)
		{
			if (addLength)
			{
				byte[] lengthBytes = BitConverter.GetBytes(source.Length);
				Array.Copy(lengthBytes, 0, dest, index, lengthBytes.Length);
				index += lengthBytes.Length;
			}

			source.CopyTo(0, dest, index, (int)source.Length);
			index += (int)source.Length;
		}
	}
}