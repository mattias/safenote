﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Storage.Streams;

namespace SafeNote.Core
{
	internal static class Scrambler
	{
		public static IList<byte[]> Scramble(IEnumerable<byte[]> buffers, string scrambler)
		{
			return buffers.Select(buffer => Scramble(buffer, scrambler)).ToList();
		}

		public static IList<IBuffer> Scramble(IEnumerable<IBuffer> buffers, string scrambler)
		{
			return buffers.Select(buffer => Scramble(buffer, scrambler)).ToList();
		}

		public static IBuffer Scramble(IBuffer data, string scrambler)
		{
			byte[] scramblerBytes = Encoding.UTF8.GetBytes(scrambler);
			
			// Get a scramble string that has the same length as the data.
			if (scramblerBytes.Length < data.Length)
			{
				// Repeat the scramble text until it's as long as the data.
				var fixedScrambler = new byte[data.Length];
				for (int i = 0; i < fixedScrambler.Length; i++)
				{
					fixedScrambler[i] = scramblerBytes[i % scramblerBytes.Length];
				}

				scramblerBytes = fixedScrambler;
			}
			else if (scramblerBytes.Length > data.Length)
			{
				// Wrap the scramble text using XOR.
				var fixedScrambler = new byte[data.Length];
				for (int i = 0; i < scramblerBytes.Length; i++)
				{
					fixedScrambler[i % fixedScrambler.Length] ^= scramblerBytes[i];
				}

				scramblerBytes = fixedScrambler;
			}

			var scrambledData = new byte[data.Length];

			for (uint i = 0; i < scrambledData.Length; i++)
			{
				scrambledData[i] = (byte)(data.GetByte(i) ^ scramblerBytes[i]);
			}

			return scrambledData.AsBuffer();
		}

		public static byte[] Scramble(byte[] data, string scrambler)
		{
			byte[] scramblerBytes = Encoding.UTF8.GetBytes(scrambler);

			// Get a scramble string that has the same length as the data.
			if (scramblerBytes.Length < data.Length)
			{
				// Repeat the scramble text until it's as long as the data.
				var fixedScrambler = new byte[data.Length];
				for (int i = 0; i < fixedScrambler.Length; i++)
				{
					fixedScrambler[i] = scramblerBytes[i % scramblerBytes.Length];
				}

				scramblerBytes = fixedScrambler;
			}
			else if (scramblerBytes.Length > data.Length)
			{
				// Wrap the scramble text using XOR.
				var fixedScrambler = new byte[data.Length];
				for (int i = 0; i < scramblerBytes.Length; i++)
				{
					fixedScrambler[i % fixedScrambler.Length] ^= scramblerBytes[i];
				}

				scramblerBytes = fixedScrambler;
			}

			var scrambledData = new byte[data.Length];
			for (int i = 0; i < scrambledData.Length; i++)
			{
				scrambledData[i] = (byte)(data[i] ^ scramblerBytes[i]);
			}

			return scrambledData;
		}
	}
}