﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Laserbrain.Safenote.Core;

namespace SafeNote.Tests
{
	public abstract class EncryptTestBase
	{
		protected string Encrypt(string plainText, string password)
		{
			using (var stream = new MemoryStream())
			{
				using (CryptoStream cryptoStream = EncryptedFile.CreateWriteStream(stream, password))
				{
					var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
					cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
				}

				stream.Flush();
				byte[] encryptedBytes = stream.ToArray();
				return Convert.ToBase64String(encryptedBytes);
			}
		}

		protected string Decrypt(string encryptedBase64, string password)
		{
			byte[] encryptedBytes = Convert.FromBase64String(encryptedBase64);

			using (var encryptedStream = new MemoryStream(encryptedBytes))
			{
				using (MemoryStream plainTextStream = EncryptedFile.CreateReadStream(encryptedStream, password))
				{
					var plainTextBytes = new byte[plainTextStream.Length];
					plainTextStream.Read(plainTextBytes, 0, plainTextBytes.Length);
					return Encoding.UTF8.GetString(plainTextBytes);
				}
			}
		}
	}
}