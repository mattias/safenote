﻿#if NETFX_CORE

using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

#else

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endif

namespace SafeNote.Tests
{
	[TestClass]
	public class EncryptedFileTest : EncryptTestBase
	{
		[TestMethod]
		public void Encrypt_HelloWorld_PW1_KeySet1()
		{
			EncryptTestConstants.UseKeySet1();
			string encrypted = Encrypt("Hello, World!", "P@ssw0rd1");
			Assert.AreEqual("U2FmZW5vdGUgAAAA2MVwyvMHPVY1Aly22viHb+a20dB0yx8jJ/QL5llNByMQAAAAWIWpqnWBHeixaRZj4oCcvhAAAAC71NiAiwumGPmS2eJ7OtHnAtAHtOYJOMpsd5phLCdsrw==", encrypted);
		}

		[TestMethod]
		public void Encrypt_HelloWorld_PW1_KeySet2()
		{
			EncryptTestConstants.UseKeySet2();
			string encrypted = Encrypt("Hello, World!", "P@ssw0rd1");
			Assert.AreEqual("U2FmZW5vdGUgAAAAkwp5sVXIFVdtRPB/AAiPY5j9uQCSrlOsGkl/Ey/zH3QQAAAAYTzNqWDpzKR1Y/1Ne8A1ihAAAACpW52z/j0mF1FiPXw+dH/9ZG+BfgnqvSuK7ZMVhprxBg==", encrypted);
		}

		[TestMethod]
		public void Encrypt_HelloWorld_PW2_KeySet1()
		{
			EncryptTestConstants.UseKeySet1();
			string encrypted = Encrypt("Hello, World!", "P@ssw0rd2");
			Assert.AreEqual("U2FmZW5vdGUgAAAA2MVwyvMHPVY2Aly22viHb+a10dB0yx8jJ/QI5llNByMQAAAAWIWpqnWBHeiyaRZj4oCcvhAAAAC71NiAiwumGPqS2eJ7OtHnAtAHtOYJOMpsd5phLCdsrw==", encrypted);
		}

		[TestMethod]
		public void Encrypt_HelloWorld_PW2_KeySet2()
		{
			EncryptTestConstants.UseKeySet2();
			string encrypted = Encrypt("Hello, World!", "P@ssw0rd2");
			Assert.AreEqual("U2FmZW5vdGUgAAAAkwp5sVXIFVduRPB/AAiPY5j+uQCSrlOsGkl8Ey/zH3QQAAAAYTzNqWDpzKR2Y/1Ne8A1ihAAAACpW52z/j0mF1JiPXw+dH/9ZG+BfgnqvSuK7ZMVhprxBg==", encrypted);
		}

		[TestMethod]
		public void Decrypt_HelloWorld_PW1_KeySet1()
		{
			string plainText = Decrypt("U2FmZW5vdGUgAAAA2MVwyvMHPVY1Aly22viHb+a20dB0yx8jJ/QL5llNByMQAAAAWIWpqnWBHeixaRZj4oCcvhAAAAC71NiAiwumGPmS2eJ7OtHnAtAHtOYJOMpsd5phLCdsrw==", "P@ssw0rd1");
			Assert.AreEqual("Hello, World!", plainText);
		}

		[TestMethod]
		public void Decrypt_HelloWorld_PW1_KeySet2()
		{
			string plainText = Decrypt("U2FmZW5vdGUgAAAAkwp5sVXIFVdtRPB/AAiPY5j9uQCSrlOsGkl/Ey/zH3QQAAAAYTzNqWDpzKR1Y/1Ne8A1ihAAAACpW52z/j0mF1FiPXw+dH/9ZG+BfgnqvSuK7ZMVhprxBg==", "P@ssw0rd1");
			Assert.AreEqual("Hello, World!", plainText);
		}

		[TestMethod]
		public void Decrypt_HelloWorld_PW2_KeySet1()
		{
			string plainText = Decrypt("U2FmZW5vdGUgAAAA2MVwyvMHPVY2Aly22viHb+a10dB0yx8jJ/QI5llNByMQAAAAWIWpqnWBHeiyaRZj4oCcvhAAAAC71NiAiwumGPqS2eJ7OtHnAtAHtOYJOMpsd5phLCdsrw==", "P@ssw0rd2");
			Assert.AreEqual("Hello, World!", plainText);
		}

		[TestMethod]
		public void Decrypt_HelloWorld_PW2_KeySet2()
		{
			string plainText = Decrypt("U2FmZW5vdGUgAAAAkwp5sVXIFVduRPB/AAiPY5j+uQCSrlOsGkl8Ey/zH3QQAAAAYTzNqWDpzKR2Y/1Ne8A1ihAAAACpW52z/j0mF1JiPXw+dH/9ZG+BfgnqvSuK7ZMVhprxBg==", "P@ssw0rd2");
			Assert.AreEqual("Hello, World!", plainText);
		}

		[TestMethod]
		public void Decrypt_HelloWorld_Wrong_PW()
		{
			try
			{
				Decrypt("U2FmZW5vdGUgAAAA2MVwyvMHPVY1Aly22viHb+a20dB0yx8jJ/QL5llNByMQAAAAWIWpqnWBHeixaRZj4oCcvhAAAAC71NiAiwumGPmS2eJ7OtHnAtAHtOYJOMpsd5phLCdsrw==",
					"P@ssw0rd2");
			}
			catch
			{
				return;
			}

			Assert.Fail();
		}
	}
}