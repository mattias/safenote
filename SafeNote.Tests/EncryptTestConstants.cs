﻿using System;

namespace SafeNote.Tests
{
	public static class EncryptTestConstants
	{
		static EncryptTestConstants()
		{
			UseKeySet1();
		}

		public static byte[] Key;
		public static byte[] Iv;

		public static void UseKeySet1()
		{
			Key = Convert.FromBase64String(Key1);
			Iv = Convert.FromBase64String(Iv1);
		}

		public static void UseKeySet2()
		{
			Key = Convert.FromBase64String(Key2);
			Iv = Convert.FromBase64String(Iv2);
		}

		public const string Key1 = "iIUDuYQ3TzIEUhzFqY+3HYKHgZAHuGgTVZA6thk+dFQ=";
		public const string Iv1 = "CMXa2QKxb4yAOVYQkfeszA==";

		public const string Key2 = "w0oKwiL4ZzNcFLAMc3+/EfzM6UDh3SScaC1OQ2+AbAM=";
		public const string Iv2 = "MXy+2hfZvsBEM70+CLcF+A==";
	}
}