﻿namespace Laserbrain.Safenote
{
    partial class NewPasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewPasswordForm));
			this.okButton = new System.Windows.Forms.Button();
			this.enterPasswordLabel = new System.Windows.Forms.Label();
			this.passwordTextBox = new System.Windows.Forms.TextBox();
			this.repeatPasswordLabel = new System.Windows.Forms.Label();
			this.repeatPasswordTextBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// okButton
			// 
			this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.okButton.Location = new System.Drawing.Point(291, 248);
			this.okButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(245, 55);
			this.okButton.TabIndex = 2;
			this.okButton.Text = "OK";
			this.okButton.UseVisualStyleBackColor = true;
			this.okButton.Click += new System.EventHandler(this.OkButtonClick);
			// 
			// enterPasswordLabel
			// 
			this.enterPasswordLabel.AutoSize = true;
			this.enterPasswordLabel.Location = new System.Drawing.Point(32, 21);
			this.enterPasswordLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
			this.enterPasswordLabel.Name = "enterPasswordLabel";
			this.enterPasswordLabel.Size = new System.Drawing.Size(211, 32);
			this.enterPasswordLabel.TabIndex = 4;
			this.enterPasswordLabel.Text = "Enter password";
			// 
			// passwordTextBox
			// 
			this.passwordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.passwordTextBox.Font = new System.Drawing.Font("Wingdings", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
			this.passwordTextBox.Location = new System.Drawing.Point(32, 60);
			this.passwordTextBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
			this.passwordTextBox.MaxLength = 1000;
			this.passwordTextBox.Name = "passwordTextBox";
			this.passwordTextBox.PasswordChar = 'l';
			this.passwordTextBox.Size = new System.Drawing.Size(756, 52);
			this.passwordTextBox.TabIndex = 0;
			this.passwordTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PasswordTextBoxKeyDown);
			// 
			// repeatPasswordLabel
			// 
			this.repeatPasswordLabel.AutoSize = true;
			this.repeatPasswordLabel.Location = new System.Drawing.Point(32, 126);
			this.repeatPasswordLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
			this.repeatPasswordLabel.Name = "repeatPasswordLabel";
			this.repeatPasswordLabel.Size = new System.Drawing.Size(235, 32);
			this.repeatPasswordLabel.TabIndex = 7;
			this.repeatPasswordLabel.Text = "Repeat password";
			// 
			// repeatPasswordTextBox
			// 
			this.repeatPasswordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.repeatPasswordTextBox.Font = new System.Drawing.Font("Wingdings", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
			this.repeatPasswordTextBox.Location = new System.Drawing.Point(32, 165);
			this.repeatPasswordTextBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
			this.repeatPasswordTextBox.MaxLength = 1000;
			this.repeatPasswordTextBox.Name = "repeatPasswordTextBox";
			this.repeatPasswordTextBox.PasswordChar = 'l';
			this.repeatPasswordTextBox.Size = new System.Drawing.Size(756, 52);
			this.repeatPasswordTextBox.TabIndex = 1;
			this.repeatPasswordTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RepeatPasswordTextBoxKeyDown);
			// 
			// NewPasswordForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(827, 331);
			this.Controls.Add(this.repeatPasswordLabel);
			this.Controls.Add(this.repeatPasswordTextBox);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.enterPasswordLabel);
			this.Controls.Add(this.passwordTextBox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "NewPasswordForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "New Password";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label enterPasswordLabel;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label repeatPasswordLabel;
        private System.Windows.Forms.TextBox repeatPasswordTextBox;

    }
}