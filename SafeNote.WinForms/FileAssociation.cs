﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Laserbrain.Safenote
{
	public static class FileAssociation
	{
		private const string FileExtension = ".safenote";
		private const string KeyName = "Safe Note";
		private const string FileDescription = "Safe Note";

		public static bool Associate()
		{
			try
			{
				Associate(
					FileExtension,
					KeyName,
					Application.ExecutablePath,
					FileDescription);

				return true;
			}
			catch
			{
				return false;
			}
		}

		public static bool IsAssociated()
		{
			return IsAssociated(
				FileExtension,
				KeyName,
				Application.ExecutablePath,
				FileDescription);
		}

		private static bool IsAssociated(string extension, string keyName, string openWith, string fileDescription)
		{
			try
			{
				DoAssociation(
					extension,
					keyName,
					openWith,
					fileDescription,
					(parent, subKey) =>
					{
						var key = parent.OpenSubKey(subKey);
						if (key == null)
						{
							throw new Exception("Missing sub key");
						}
						return key;
					},
					(key, name, expectedValue, comparison) =>
					{
						string actualValue = (string)key.GetValue(name);

						if (!string.Equals(actualValue, expectedValue, comparison))
						{
							throw new Exception("Wrong value");
						}
					});

				return true;
			}
			catch
			{
				return false;
			}
		}

		private static void Associate(string extension, string keyName, string openWith, string fileDescription)
		{
			DoAssociation(
				extension,
				keyName,
				openWith,
				fileDescription,
				(parent, subKey) =>
				{
					parent.DeleteSubKeyTree(subKey, false);
					var key = parent.CreateSubKey(subKey);
					if (key == null)
					{
						throw new Exception("Couldn't create sub key");
					}
					return key;
				},
				(key, name, value, comparison) => key.SetValue(name, value));
		}

		private static void DoAssociation(
			string extension,
			string keyName,
			string openWith,
			string fileDescription,
			Func<RegistryKey, string, RegistryKey> getRegistryKey,
			Action<RegistryKey, string, string, StringComparison> handleValue)
		{
			using (RegistryKey baseKey = getRegistryKey(Registry.ClassesRoot, extension))
			{
				handleValue(baseKey, "", keyName, StringComparison.Ordinal);
			}

			using (RegistryKey openMethod = getRegistryKey(Registry.ClassesRoot, keyName))
			{
				handleValue(openMethod, "", fileDescription, StringComparison.Ordinal);

				using (RegistryKey defaultIcon = getRegistryKey(openMethod, "DefaultIcon"))
				{
					handleValue(defaultIcon, "", "\"" + openWith + "\",0", StringComparison.OrdinalIgnoreCase);
				}

				using (RegistryKey shell = getRegistryKey(openMethod, "Shell"))
				{
					using (RegistryKey open = getRegistryKey(shell, "open"))
					{
						using (RegistryKey command = getRegistryKey(open, "command"))
						{
							handleValue(command, "", "\"" + openWith + "\"" + " \"%1\"", StringComparison.OrdinalIgnoreCase);
						}
					}
				}
			}

			using (RegistryKey currentUser = getRegistryKey(Registry.CurrentUser, @"Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\" + extension))
			{
				using (RegistryKey userChoice = getRegistryKey(currentUser, "UserChoice"))
				{
					handleValue(userChoice, "Progid", keyName, StringComparison.Ordinal);
				}
			}
		}
	}
}