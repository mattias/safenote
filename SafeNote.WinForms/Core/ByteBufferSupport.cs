﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Laserbrain.Safenote.Core
{
	internal static class ByteBufferSupport
	{
		private static readonly int LengthLength = BitConverter.GetBytes(0).Length;

		public static void WriteBuffer(Stream stream, byte[] data)
		{
			stream.Write(BitConverter.GetBytes(data.Length), 0, LengthLength);
			stream.Write(data, 0, data.Length);
		}

		public static IList<byte[]> SplitBuffers(Stream stream, int maxBufferCount)
		{
			var buffers = new List<byte[]>();
			long offset = stream.Position;

			do
			{
				if (offset + LengthLength > stream.Length)
				{
					break;
				}

				var lengthBytes = new byte[LengthLength];
				stream.Read(lengthBytes, 0, LengthLength);
				int bufferLength = BitConverter.ToInt32(lengthBytes, 0);
				offset += LengthLength;
				if (offset + bufferLength > stream.Length)
				{
					break;
				}

				var buffer = new byte[bufferLength];
				stream.Read(buffer, 0, bufferLength);
				buffers.Add(buffer);
				offset += bufferLength;
			}
			while ((offset < stream.Length) && (buffers.Count < maxBufferCount));

			return buffers;
		}
	}
}