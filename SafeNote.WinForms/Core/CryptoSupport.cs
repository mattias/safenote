﻿using System.IO;
using System.Security.Cryptography;

namespace Laserbrain.Safenote.Core
{
	internal static class CryptoSupport
	{
		public static SymmetricAlgorithm Create()
		{
			return new RijndaelManaged { Mode = CipherMode.CBC };
		}

		public static byte[] Encrypt(SymmetricAlgorithm algorithm, byte[] data)
		{
			using (var ms = new MemoryStream())
			{
				using (var cs = new CryptoStream(ms, algorithm.CreateEncryptor(), CryptoStreamMode.Write))
				{
					// Write the des key byte array into the crypto stream.
					cs.Write(data, 0, data.Length);
					cs.FlushFinalBlock();

					// Get the encrypted data back from the memory stream.
					return ms.ToArray();
				}
			}
		}

		public static byte[] Decrypt(SymmetricAlgorithm algorithm, byte[] data)
		{
			using (var ms = new MemoryStream())
			{
				using (var cs = new CryptoStream(ms, algorithm.CreateDecryptor(), CryptoStreamMode.Write))
				{
					// Flush the data through the crypto stream into the memory stream
					cs.Write(data, 0, data.Length);
					cs.FlushFinalBlock();

					// Get the data back from the memory stream.
					return ms.ToArray();
				}
			}
		}
	}
}