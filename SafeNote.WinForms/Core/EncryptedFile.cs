﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Laserbrain.Safenote.Core
{
	internal static class EncryptedFile
	{
		public const string DefaultExtension = ".safenote";

		private static readonly byte[] FileHeader = Encoding.ASCII.GetBytes("Safenote");

		public static CryptoStream CreateWriteStream(Stream stream, string password)
		{
			IList<byte[]> cryptoKeySet = ScrambledCrypto.CreateCryptoKeySet(password);
			SymmetricAlgorithm algorithm = ScrambledCrypto.CreateAlgorithm(cryptoKeySet, password);

			stream.Write(FileHeader, 0, FileHeader.Length);
			foreach (byte[] cryptoKeyItem in cryptoKeySet)
			{
				ByteBufferSupport.WriteBuffer(stream, cryptoKeyItem);
			}

			return new CryptoStream(stream, algorithm.CreateEncryptor(), CryptoStreamMode.Write);
		}

		public static MemoryStream CreateReadStream(Stream stream, string password)
		{
			// Verify file header
			var actualFileHeader = new byte[FileHeader.Length];
			stream.Read(actualFileHeader, 0, actualFileHeader.Length);
			if (FileHeader.Where((t, i) => actualFileHeader[i] != t).Any())
			{
				throw new FormatException();
			}

			IList<byte[]> buffers = ByteBufferSupport.SplitBuffers(stream, 3);
			if (buffers.Count != 3)
			{
				throw new FormatException();
			}

			SymmetricAlgorithm algorithm = ScrambledCrypto.CreateAlgorithm(buffers, password);

			// Decrypt into a memorystream since a CryptoStream doesn't support seeking.
			var memoryStream = new MemoryStream();
			using (var cryptoStream = new CryptoStream(stream, algorithm.CreateDecryptor(), CryptoStreamMode.Read))
			{
				var buffer = new byte[8192];
				int length;
				do
				{
					length = cryptoStream.Read(buffer, 0, buffer.Length);
					if (length == 0)
					{
						break;
					}

					memoryStream.Write(buffer, 0, length);
				}
				while (length == buffer.Length);
			}

			memoryStream.Position = 0;
			return memoryStream;
		}
	}
}