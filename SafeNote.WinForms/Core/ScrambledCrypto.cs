﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Laserbrain.Safenote.Core
{
	internal static class ScrambledCrypto
	{
		private const string CryptoKeyVerifyString = "Verified OK";

		public static IList<byte[]> CreateCryptoKeySet(string scrambler)
		{
			SymmetricAlgorithm algorithm = CryptoSupport.Create();

#if TEST
			algorithm.Key = SafeNote.Tests.EncryptTestConstants.Key;
			algorithm.IV = SafeNote.Tests.EncryptTestConstants.Iv;
#else
			algorithm.GenerateKey();
			algorithm.GenerateIV();
#endif

			byte[] scrambledKey = Scrambler.Scramble(algorithm.Key, scrambler);
			byte[] scrambledIv = Scrambler.Scramble(algorithm.IV, scrambler);
			byte[] encryptedVerificationString = Scrambler.Scramble(CryptoSupport.Encrypt(algorithm, Encoding.UTF8.GetBytes(CryptoKeyVerifyString)), scrambler);
			return new List<byte[]> { scrambledKey, scrambledIv, encryptedVerificationString };
		}

		public static SymmetricAlgorithm CreateAlgorithm(IList<byte[]> cryptoKeySet, string scrambler)
		{
			if (cryptoKeySet.Count < 3)
			{
				throw new ArgumentException("Invalid cryptoKeySet.");
			}

			byte[] scrambledKey = cryptoKeySet[0];
			byte[] scrambledIv = cryptoKeySet[1];
			byte[] encryptedVerificationString = cryptoKeySet[2];

			SymmetricAlgorithm algorithm = CryptoSupport.Create();
			algorithm.Key = Scrambler.Scramble(scrambledKey, scrambler);
			algorithm.IV = Scrambler.Scramble(scrambledIv, scrambler);

			byte[] verifyStringBytes;
			try
			{
				verifyStringBytes = CryptoSupport.Decrypt(algorithm, Scrambler.Scramble(encryptedVerificationString, scrambler));
			}
			catch (CryptographicException cryptographicException)
			{
				throw new ArgumentException("Invalid scrambler.", cryptographicException);
			}

			string verifyString = Encoding.UTF8.GetString(verifyStringBytes, 0, verifyStringBytes.Length);
			if (!verifyString.Equals(CryptoKeyVerifyString))
			{
				throw new ArgumentException("Invalid scrambler.");
			}

			return algorithm;
		}
	}
}