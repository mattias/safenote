﻿using Laserbrain.Safenote.Core;

namespace Laserbrain.Safenote
{
	using System;
	using System.IO;
	using System.Reflection;
	using System.Windows.Forms;

	internal partial class MainForm : Form
	{
		private readonly string version;
		private string currentPath;
		private string currentPassword;
		private bool unsaved;

		private enum SaveKind
		{
			Save,
			SaveAs,
			ChangePassword,
		}

		public MainForm()
		{
			InitializeComponent();

			var name = Assembly.GetExecutingAssembly().GetName();
			if (name.Version != null)
			{
				version = string.Format("{0}.{1}", name.Version.Major, name.Version.Minor);
			}

			NewDocument();

			var args = Environment.GetCommandLineArgs();
			if ((args.Length > 1) && File.Exists(args[1]) && !OpenDocument(args[1]))
			{
				AbortRunning = true;
			}

			Width = (int)(Screen.PrimaryScreen.WorkingArea.Width * 0.7);
			Height = (int)(Screen.PrimaryScreen.WorkingArea.Height * 0.7);
		}

		public bool AbortRunning
		{ get; private set; }

		private void NewToolStripMenuItemClick(object sender, EventArgs e)
		{
			NewDocument();
		}

		private void OpenToolStripMenuItemClick(object sender, EventArgs e)
		{
			OpenDocument();
		}

		private void SaveToolStripMenuItemClick(object sender, EventArgs e)
		{
			SaveDocument(SaveKind.Save);
		}

		private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
		{
			SaveDocument(SaveKind.SaveAs);
		}

		private void ExitToolStripMenuItemClick(object sender, EventArgs e)
		{
			CloseApplication();
		}

		private void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			if (!EnsureSaved())
			{
				e.Cancel = true;
			}
		}

		private void CloseApplication()
		{
			if (!EnsureSaved())
			{
				return;
			}

			Close();
		}

		private void NewDocument()
		{
			if (!EnsureSaved())
			{
				return;
			}

			currentPath = null;
			currentPassword = null;
			changePasswordToolStripMenuItem.Enabled = false;
			mainRichTextBox.Clear();
			unsaved = false;
			UpdateTitle();
		}

		private void MainRichTextBoxTextChanged(object sender, EventArgs e)
		{
			unsaved = true;
		}

		private void UpdateTitle()
		{
			Text = string.Format("Safenote {0} - {1}", version, (currentPath == null) ? "Untitled" : Path.GetFileName(currentPath));
		}

		private void OpenDocument()
		{
			if (!EnsureSaved())
			{
				return;
			}

			const string ErrorMessage = "Error while opening the file.";
			try
			{
				using (var dialog = new OpenFileDialog())
				{
					dialog.Title = "Open";
					dialog.Filter = string.Format(
						"Safenote files (*{0})|*{0}|All files (*.*)|*.*",
						EncryptedFile.DefaultExtension);
					if (dialog.ShowDialog() == DialogResult.OK)
					{
						OpenDocument(dialog.FileName);
					}
				}
			}
			catch
			{
				MessageBox.Show(this, ErrorMessage, "Safenote", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private bool OpenDocument(string path)
		{
			var errorMessage = "Error while reading the file.";
			try
			{
				using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
				{
					var password = ValidatePasswordForm.ShowDialog(this);
					if (password != null)
					{
						errorMessage = "Error while decrypting the file.";
						using (var cryptoStream = EncryptedFile.CreateReadStream(fileStream, password))
						{
							mainRichTextBox.LoadFile(cryptoStream, RichTextBoxStreamType.RichText);
							currentPath = path;
							currentPassword = password;
							changePasswordToolStripMenuItem.Enabled = true;
							unsaved = false;
							UpdateTitle();
							return true;
						}
					}
				}
			}
			catch
			{
				MessageBox.Show(this, errorMessage, "Safenote", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			return false;
		}

		private bool SaveDocument(SaveKind saveKind)
		{
			var errorMessage = "Error while saving the file.";
			try
			{
				if ((saveKind == SaveKind.Save) && !unsaved)
				{
					return true;
				}

				string path;
				if ((saveKind != SaveKind.SaveAs) && (currentPath != null))
				{
					path = currentPath;
				}
				else
				{
					using (var dialog = new SaveFileDialog())
					{
						dialog.Title = "Save";
						dialog.Filter = string.Format("Safenote files (*{0})|*{0}|All files (*.*)|*.*", EncryptedFile.DefaultExtension);
						if (dialog.ShowDialog() == DialogResult.OK)
						{
							path = dialog.FileName;
						}
						else
						{
							return false;
						}
					}
				}

				string password;
				if (saveKind == SaveKind.ChangePassword)
				{
					password = NewPasswordForm.ShowDialog(this, string.Empty);
				}
				else if ((saveKind == SaveKind.Save) && (currentPassword != null))
				{
					password = currentPassword;
				}
				else
				{
					password = NewPasswordForm.ShowDialog(this, currentPassword);
				}

				if (password != null)
				{
					errorMessage = "Error while writing to the file.";
					using (Stream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
					{
						errorMessage = "Error while encrypting the file.";
						using (var cryptoStream = EncryptedFile.CreateWriteStream(fileStream, password))
						{
							mainRichTextBox.SaveFile(cryptoStream, RichTextBoxStreamType.RichText);
							cryptoStream.FlushFinalBlock();
							currentPath = path;
							currentPassword = password;
							changePasswordToolStripMenuItem.Enabled = true;
							unsaved = false;
							UpdateTitle();
							return true;
						}
					}
				}
			}
			catch
			{
				MessageBox.Show(this, errorMessage, "Safenote", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			return false;
		}

		private bool EnsureSaved()
		{
			if (!unsaved)
			{
				return true;
			}

			var result = MessageBox.Show(this, string.Format("Save the changes to {0}?", currentPath), "Safenote", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
			switch (result)
			{
				case DialogResult.Yes:
					return SaveDocument(SaveKind.Save);
				case DialogResult.No:
					return true;
			}

			return false;
		}

		private void ChangePasswordToolStripMenuItemClick(object sender, EventArgs e)
		{
			SaveDocument(SaveKind.ChangePassword);
		}
	}
}