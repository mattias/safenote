﻿namespace Laserbrain.Safenote
{
	using System;
	using System.Windows.Forms;

	internal partial class NewPasswordForm : Form
	{
		private NewPasswordForm()
		{
			InitializeComponent();
		}

		public static string ShowDialog(IWin32Window owner, string password)
		{
			using (var form = new NewPasswordForm())
			{
				do
				{
					if (password != null)
					{
						form.passwordTextBox.Text = password;
						form.repeatPasswordTextBox.Text = password;
					}

					if (form.ShowDialog(owner) == DialogResult.OK)
					{
						if (form.passwordTextBox.Text == form.repeatPasswordTextBox.Text)
						{
							if (form.passwordTextBox.Text.Length > 0)
							{
								return form.passwordTextBox.Text;
							}

							MessageBox.Show(
								form,
								"You must have at least one character long password.",
								"Wrong password",
								MessageBoxButtons.OK,
								MessageBoxIcon.Error);
						}
						else
						{
							MessageBox.Show(
								form,
								"You didn't write the same password twice.",
								"Wrong password",
								MessageBoxButtons.OK,
								MessageBoxIcon.Error);
						}
					}
					else
					{
						return null;
					}
				}
				while (true);
			}
		}

		private void OkButtonClick(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
		}

		private void PasswordTextBoxKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode != Keys.Return)
			{
				return;
			}

			e.Handled = true;
			e.SuppressKeyPress = true;
			repeatPasswordTextBox.Focus();
		}

		private void RepeatPasswordTextBoxKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode != Keys.Return)
			{
				return;
			}

			e.Handled = true;
			e.SuppressKeyPress = true;
			DialogResult = DialogResult.OK;
		}
	}
}