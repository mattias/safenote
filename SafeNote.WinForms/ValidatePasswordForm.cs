﻿namespace Laserbrain.Safenote
{
	using System;
	using System.Windows.Forms;

	internal partial class ValidatePasswordForm : Form
	{
		private ValidatePasswordForm()
		{
			InitializeComponent();
		}

		public static new string ShowDialog(IWin32Window owner)
		{
			using (var form = new ValidatePasswordForm())
			{
				if (((Form)form).ShowDialog(owner) == DialogResult.OK)
				{
					return form.passwordTextBox.Text;
				}
			}

			return null;
		}

		private void PasswordTextBoxKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode != Keys.Return)
			{
				return;
			}

			e.Handled = true;
			e.SuppressKeyPress = true;
			DialogResult = DialogResult.OK;
		}

		private void OkButtonClick(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
		}
	}
}