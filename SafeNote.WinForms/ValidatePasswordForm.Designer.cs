﻿namespace Laserbrain.Safenote
{
    partial class ValidatePasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValidatePasswordForm));
			this.passwordTextBox = new System.Windows.Forms.TextBox();
			this.enterPasswordLabel = new System.Windows.Forms.Label();
			this.okButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// passwordTextBox
			// 
			this.passwordTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.passwordTextBox.Font = new System.Drawing.Font("Wingdings", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
			this.passwordTextBox.Location = new System.Drawing.Point(32, 60);
			this.passwordTextBox.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
			this.passwordTextBox.MaxLength = 1000;
			this.passwordTextBox.Name = "passwordTextBox";
			this.passwordTextBox.PasswordChar = 'l';
			this.passwordTextBox.Size = new System.Drawing.Size(756, 52);
			this.passwordTextBox.TabIndex = 0;
			this.passwordTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PasswordTextBoxKeyDown);
			// 
			// enterPasswordLabel
			// 
			this.enterPasswordLabel.AutoSize = true;
			this.enterPasswordLabel.Location = new System.Drawing.Point(32, 21);
			this.enterPasswordLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
			this.enterPasswordLabel.Name = "enterPasswordLabel";
			this.enterPasswordLabel.Size = new System.Drawing.Size(211, 32);
			this.enterPasswordLabel.TabIndex = 1;
			this.enterPasswordLabel.Text = "Enter password";
			// 
			// okButton
			// 
			this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.okButton.Location = new System.Drawing.Point(291, 138);
			this.okButton.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(245, 55);
			this.okButton.TabIndex = 2;
			this.okButton.Text = "OK";
			this.okButton.UseVisualStyleBackColor = true;
			this.okButton.Click += new System.EventHandler(this.OkButtonClick);
			// 
			// ValidatePasswordForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(827, 222);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.enterPasswordLabel);
			this.Controls.Add(this.passwordTextBox);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ValidatePasswordForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Password";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label enterPasswordLabel;
        private System.Windows.Forms.Button okButton;
    }
}