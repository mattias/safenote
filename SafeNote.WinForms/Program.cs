﻿using System;
using System.Windows.Forms;

namespace Laserbrain.Safenote
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			FileAssociation.Associate();

			using (var form = new MainForm())
			{
				if (!form.AbortRunning)
				{
					Application.Run(form);
				}
			}
		}
	}
}
