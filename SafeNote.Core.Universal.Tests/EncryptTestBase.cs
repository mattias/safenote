﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using SafeNote.Core;
using Windows.Storage.Streams;

namespace SafeNote.Tests
{
	public abstract class EncryptTestBase
	{
		protected string Encrypt(string plainText, string password)
		{
			IBuffer plainTextBuffer = Encoding.UTF8.GetBytes(plainText).AsBuffer();
			IBuffer encryptedBuffer = EncryptedFile.Encrypt(plainTextBuffer, password);
			return Convert.ToBase64String(encryptedBuffer.ToArray());
		}

		protected string Decrypt(string encryptedBase64, string password)
		{
			IBuffer encryptedBuffer = Convert.FromBase64String(encryptedBase64).AsBuffer();
			IBuffer plainTextBuffer = EncryptedFile.Decrypt(encryptedBuffer, password);
			byte[] plainTextBytes = plainTextBuffer.ToArray();
			return Encoding.UTF8.GetString(plainTextBytes, 0, plainTextBytes.Length);
		}
	}
}